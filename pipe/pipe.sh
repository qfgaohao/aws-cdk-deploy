#!/usr/bin/env bash
#
# Deploy to AWS S3, http://aws.amazon.com/s3/
#
# Required globals:
#   AWS_ACCESS_KEY_ID
#   AWS_SECRET_ACCESS_KEY
#   AWS_DEFAULT_REGION
#   APP_ROOT
#
# Optional globals:
#   EXTRA_ARGS
#   DEBUG

source "$(dirname "$0")/common.sh"

# mandatory parameters
APP_ROOT=${APP_ROOT:?'APP_ROOT variable missing.'}


default_authentication() {
  info "Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY."
  AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID:?'AWS_ACCESS_KEY_ID variable missing.'}
  AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY:?'AWS_SECRET_ACCESS_KEY variable missing.'}
}

oidc_authentication() {
  info "Authenticating with a OpenID Connect (OIDC) Web Identity Provider."
      mkdir -p /.aws-oidc
      AWS_WEB_IDENTITY_TOKEN_FILE=/.aws-oidc/web_identity_token
      echo "${BITBUCKET_STEP_OIDC_TOKEN}" >> ${AWS_WEB_IDENTITY_TOKEN_FILE}
      chmod 400 ${AWS_WEB_IDENTITY_TOKEN_FILE}
      aws configure set web_identity_token_file ${AWS_WEB_IDENTITY_TOKEN_FILE}
      aws configure set role_arn ${AWS_OIDC_ROLE_ARN}
      unset AWS_ACCESS_KEY_ID
      unset AWS_SECRET_ACCESS_KEY
}

setup_authentication() {
  enable_debug
  AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION:?'AWS_DEFAULT_REGION variable missing.'}
  if [[ -n "${AWS_OIDC_ROLE_ARN}" ]]; then
    if [[ -n "${BITBUCKET_STEP_OIDC_TOKEN}" ]]; then
      oidc_authentication
    else
      warning 'Parameter `oidc: true` in the step configuration is required for OIDC authentication'
      default_authentication
    fi
  else
    default_authentication
  fi
}

setup_authentication

preexecution_hook() {
  if [[ -n "${PRE_EXECUTION_SCRIPT}" ]]; then
    if [[ ! -f "${PRE_EXECUTION_SCRIPT}" ]]; then
      fail "$PRE_EXECUTION_SCRIPT preexecution hook file doesn't exist."
    fi
    ./$PRE_EXECUTION_SCRIPT
  fi
}


preexecution_hook

if [[ ! -d "${APP_ROOT}" ]]; then
  fail "$APP_ROOT directory doesn't exist."
fi

# default parameters
EXTRA_ARGS=${EXTRA_ARGS:=""}

AWS_DEBUG_ARGS=""
if [[ "${DEBUG}" == "true" ]]; then
  info "Enabling debug mode."
  AWS_DEBUG_ARGS="--debug"
fi

info "Starting deployment: cdk deploy"
run cd $APP_ROOT; cdk deploy
if [[ "${status}" -eq 0 ]]; then
  success "Deployment successful."
else
  fail "Deployment failed."
fi
