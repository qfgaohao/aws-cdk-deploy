FROM amazon/aws-cli:2.2.13

RUN amazon-linux-extras install python3
RUN python3 -m pip install aws-cdk-lib

RUN curl --silent --location https://rpm.nodesource.com/setup_14.x | bash -
RUN yum -y install nodejs

RUN npm install -g aws-cdk

RUN curl -fsSL -o /common.sh https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.6.0/common.sh

COPY pipe /
COPY LICENSE.txt README.md pipe.yml /

ENTRYPOINT ["/pipe.sh"]
